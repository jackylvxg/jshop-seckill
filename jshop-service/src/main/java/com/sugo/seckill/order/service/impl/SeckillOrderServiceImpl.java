package com.sugo.seckill.order.service.impl;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.github.pagehelper.Page;

import com.sugo.seckill.aop.lock.ServiceLock;
import com.sugo.seckill.aop.redis.ServiceRedisLock;
import com.sugo.seckill.aop.zk.ServiceZkLock;
import com.sugo.seckill.error.BaseException;
import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.http.HttpStatus;
import com.sugo.seckill.lock.distributedlock.redis.RedissLockUtil;
import com.sugo.seckill.mapper.order.SeckillGoodsMapper;
import com.sugo.seckill.mapper.order.SeckillOrderMapper;
import com.sugo.seckill.mapper.pay.TbPayLogMapper;
import com.sugo.seckill.mq.MqProducer;
import com.sugo.seckill.order.service.SeckillOrderService;
import com.sugo.seckill.page.PageResult;
import com.sugo.seckill.pojo.*;
import com.sugo.seckill.queue.jvm.SeckillQueue;
import com.sugo.seckill.utils.IdWorker;
import com.sun.corba.se.pept.transport.ReaderThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.github.pagehelper.PageHelper;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {

	@Autowired
	private SeckillOrderMapper seckillOrderMapper;

	//注入支付
	@Autowired
	private TbPayLogMapper payLogMapper;


	//注入商品对象
	@Autowired
	private SeckillGoodsMapper seckillGoodsMapper;

	//注入redis
	@Autowired
	private RedisTemplate redisTemplate;

	//注入Idworker
	@Autowired
	private IdWorker idWorker;

	@Autowired
	private MqProducer producer;


	//程序锁
	//互斥锁 参数默认false，不公平锁
	private Lock lock = new ReentrantLock(true);

	//日志
	private final static Logger LOGGER = LoggerFactory.getLogger(SeckillOrderServiceImpl.class);



	// disruptor 队列 --- 600w /s
	// 分布式队列
	// redis
	// mq


	public void getSeckillGoods(Long seckillId){

		redisTemplate.delete("seckill_goods_"+seckillId);
		redisTemplate.delete("seckill_goods_stock_"+seckillId);

		//创建example对象
		Example example = new Example(TbSeckillGoods.class);
		Example.Criteria criteria = example.createCriteria();

		/*//设置查询条件
		//状态可用
		criteria.andEqualTo("status",1);
		//时间必须在活动区间
		criteria.andCondition("now() BETWEEN start_time_date AND end_time_date");
		//库存必须大于0
		criteria.andGreaterThan("stockCount",0);
		criteria.andEqualTo("id",1);

		//获取Redis中所有的key,实现排除当前Redis中已经存在的商品
		Set<Long> ids = redisTemplate.boundHashOps("seckillGoods").keys();
		//判断
		if(ids!=null && ids.size()>0){
			criteria.andNotIn("id",ids);
		}

		//查询
		List<TbSeckillGoods> seckillGoodsList = seckillGoodsMapper.selectByExample(example);
*/
		TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(seckillId);

		//判断是否有入库商品
		//if(seckillGoodsList!=null && seckillGoodsList.size()>0){
			//循环
			//for (TbSeckillGoods goods : seckillGoodsList) {

				//放入商品对象
				redisTemplate.opsForValue().set("seckill_goods_"+seckillId,seckillGoods);
				//存储库存
				redisTemplate.opsForValue().set("seckill_goods_stock_"+seckillId,seckillGoods.getStockCount());

               /* //存储到redis
                redisTemplate.boundHashOps("seckillGoods").put(String.valueOf(goods.getId()),goods);
                //存储库存:剩余库存存，用来防止超卖
                redisTemplate.boundHashOps("seckillGoodsCount").put(String.valueOf(goods.getId()),goods.getStockCount());*/

			//}
		//}
	}

	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceLock
	@Override
	public HttpResult startKilled(Long killId, String userId) {

		try {
			//实现一个加锁的动作

			//lock.lock();

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
                return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
            }
			if(seckillGoods.getStatus() != 1){
                return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
            }
			if(seckillGoods.getStockCount() <= 0){
                return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
            }
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
                return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
            }
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
                return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
            }

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			lock.unlock();
		}*/
		return null;
	}

	/**
	 * @Description: 分布式锁下单，MySQL悲观锁的方式
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledWithMysqlLock(Long killId, String userId) {
		try {
			// 从数据库查询商品数据,使用数据库的悲观锁
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKeyBySQLLock(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: 分布式锁下单，MySQL乐观锁的方式
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledWithMysqlVersion(Long killId, String userId) {
		try {
			// 从数据库查询商品数据,使用数据库的悲观锁
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			// update
			seckillGoodsMapper.updateSeckillGoodsByPrimaryKeyByVersion(killId,seckillGoods.getVersion());


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: 分布式锁，Redis的分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceRedisLock
	@Override
	public HttpResult startKilledWithRedisLock(Long killId, String userId) {
		try {
			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: 分布式锁，zk的分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceZkLock
	@Override
	public HttpResult startKilledWithZkLock(Long killId, String userId) {
		try {
			// 从数据库查询商品数据,使用数据库的悲观锁
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKeyBySQLLock(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: 下单性能优化
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledWithMore(Long killId, String userId) throws BaseException{
		try {
			// 1、优化一：从缓存中查询商品数据
			TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate.opsForValue().get("seckill_goods_" + killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

		   // 优化二： 从缓存中扣减库存
			boolean res = reduceStock(killId);
			// 扣减库存失败
			if(!res){
				throw new BaseException(HttpStatus.SEC_GOODS_STOCK_FAIL,"扣减库存失败");
			}

			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());


			// 优化三：下单异步，使用队列进行异步化的操作
			// BlockingQueue,disruptor,rocketMQ
			Boolean succ = SeckillQueue.getMailQueue().produce(order);

			if(!succ){
				throw new BaseException(HttpStatus.SEC_GOODS_STOCK_FAIL,"下单失败");
			}

			// 下单成功，消息状态
			seckillGoods.setStockCount(null);
			// 下单成功，消息可以是commit状态
			seckillGoods.setTransactionStatus(1);
			// 更新状态
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseException(HttpStatus.SEC_GOODS_STOCK_FAIL,"下单失败");
		}
	}

	/**
	 * @Description: 扣减库存
	 * @Author: hubin
	 * @CreateDate: 2021/2/1 22:03
	 * @UpdateUser: hubin
	 * @UpdateDate: 2021/2/1 22:03
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	private boolean reduceStock(Long killId) {

		// 原子操作，要么成功，要么失败
		Long in = redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, -1);
		if(in >=0){
			return true;
		}else {
			redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, 1);
			return false;
		}

	}


	/**
	 * 按分页查询
	 */
	@Override
	@Transactional
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbSeckillOrder> page=   (Page<TbSeckillOrder>) seckillOrderMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}


	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	@Transactional
	public TbSeckillOrder findOne(Long id){
		return seckillOrderMapper.selectByPrimaryKey(id);
	}

	/**
	 * @Description: 支付完毕，更新订单的状态
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 22:20
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 22:20
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	@Transactional
	public void updateOrderStatus(String out_trade_no, String transaction_id) {
		//1.修改支付日志状态

		TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
		payLog.setPayTime(new Date());//支付时间
		payLog.setTradeState("1");//交易状态
		payLog.setTransactionId(transaction_id);//流水号
		payLogMapper.updateByPrimaryKey(payLog);

		//2.修改订单状态
		String orderList = payLog.getOrderList();
		String[] ids = orderList.split(",");//订单号
		for(String id:ids){

			TbSeckillOrder order = seckillOrderMapper.selectByPrimaryKey( Long.valueOf(id) );
			order.setStatus("2");//支付状态
			order.setPayTime(new Date());//支付时间
			seckillOrderMapper.updateByPrimaryKey(order);
		}
	}


	@Override
	public TbPayLog searchPayLogFromRedis(String userId) {
		return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
	}

	/**
	 * @Description: 根据token查询用户信息
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 10:47
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 10:47
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public FrontUser getUserInfoFromRedis(String token) {
		FrontUser user =
				(FrontUser) redisTemplate.opsForValue().get(token);
		return user;
	}

	@Override
	public HttpResult getOrderMoney(Long orderId) {
		TbSeckillOrder order = seckillOrderMapper.selectByPrimaryKey(orderId);
		HttpResult httpResult = new HttpResult();
		httpResult.setData(order.getMoney());
		return httpResult;
	}

	@Override
	public TbSeckillOrder findOrderById(Long orderId) {
		return seckillOrderMapper.selectByPrimaryKey(orderId);
	}
}
