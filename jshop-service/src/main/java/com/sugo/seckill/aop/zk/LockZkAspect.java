package com.sugo.seckill.aop.zk;

import com.sugo.seckill.lock.distributedlock.zookeeper.ZkLockUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName LockAspect
 * @Description
 * @Author hubin
 * @Date 2021/1/29 23:13
 * @Version V1.0
 **/
@Component
@Scope
@Aspect
@Order(1)
public class LockZkAspect {

    // service 切入点
    @Pointcut("@annotation(com.sugo.seckill.aop.zk.ServiceZkLock)")
    public void lockAspect(){

    }

    @Around("lockAspect()")
    public Object around(ProceedingJoinPoint joinPoint){

        Object obj = null;
        // 开始加锁 -- 方法增强
        boolean res = ZkLockUtil.acquire(10, TimeUnit.SECONDS);

        try {
            if(res) {
                //执行业务
                obj = joinPoint.proceed();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }finally {
            // 释放锁
           if(res){
               ZkLockUtil.release();
           }
        }

        return obj;
    }




}

