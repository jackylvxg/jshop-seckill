package com.sugo.seckill.aop.redis;

import java.lang.annotation.*;

/**
 * @ClassName ServiceZkLock
 * @Description
 * @Author hubin
 * @Date 2021/1/29 23:12
 * @Version V1.0
 **/
@Target({ElementType.PARAMETER,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceRedisLock {

    String descripiton() default "";


}
